using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public Rigidbody bullet;
    public Transform bulletSpawnPoint;
    public float force = 200f;

    private float timer = 0f;
    public float cooldown = 5f;

    private Vector3 targetAim = Vector3.zero;
    


    void Start()
    {
        transform.LookAt(targetAim);
        transform.RotateAround(transform.position, transform.right, 25);
    }

    void FixedUpdate()
    {
        //Shoot
        timer += Time.fixedDeltaTime;
        if (timer > cooldown)
        {
            Shoot();
            timer = 0f;
        }

        //Rotate
        transform.RotateAround(transform.position, 3*transform.up + transform.forward, 40 * Time.fixedDeltaTime);
    }

    void Shoot()
    {
        Rigidbody bulletSpawned = Instantiate(bullet, bulletSpawnPoint.position, Quaternion.identity);
        bulletSpawned.AddForce(transform.up * force, ForceMode.Impulse);
    }

}
