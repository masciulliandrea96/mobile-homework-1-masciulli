using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakToSmallerObj : MonoBehaviour
{
    public Rigidbody smallerObject;

    // Obj starts with initial invulnerability
    private bool invulnerable = true;
    private float invulnerabilityTimer = 0f;
    private float invulnerabilityTime = 0.3f;

    void FixedUpdate()
    {
        // Deplete invulnerability with time
        if (invulnerable)
        {
            invulnerabilityTimer += Time.fixedDeltaTime;
            if (invulnerabilityTimer > invulnerabilityTime)
            {
                invulnerable = false;
            }
        }
       
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (invulnerable == false)
        {
            SpawnSmallerObj(collision);
            Destroy(gameObject);
        }

    }

    private void SpawnSmallerObj(Collision collision)
    {
        // Positions of the new smaller Objects, relative to parent
        Vector3[] smallerObjectPositions = new Vector3[7];
        float spawnDistance = smallerObject.transform.lossyScale.x/2;
        smallerObjectPositions[0] = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        smallerObjectPositions[1] = new Vector3(transform.position.x + spawnDistance, transform.position.y, transform.position.z);
        smallerObjectPositions[2] = new Vector3(transform.position.x, transform.position.y + spawnDistance, transform.position.z);
        smallerObjectPositions[3] = new Vector3(transform.position.x, transform.position.y, transform.position.z + spawnDistance);
        smallerObjectPositions[4] = new Vector3(transform.position.x - spawnDistance, transform.position.y, transform.position.z);
        smallerObjectPositions[5] = new Vector3(transform.position.x, transform.position.y - spawnDistance, transform.position.z);
        smallerObjectPositions[6] = new Vector3(transform.position.x, transform.position.y, transform.position.z - spawnDistance);

        // Spawn smaller Objects
        for (int i = 0; i <= 6; i++)
        {
            // Instantiate smaller objects with position and velocity
            Rigidbody so = Instantiate(smallerObject, smallerObjectPositions[i], transform.rotation);
            so.velocity = this.GetComponent<Rigidbody>().velocity;
            so.angularVelocity = this.GetComponent<Rigidbody>().angularVelocity;

        }
    }

}




